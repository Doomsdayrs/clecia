package app.clecia.matrix.datasource.impl

import app.clecia.matrix.common.ext.cleciaTransaction
import app.clecia.matrix.datasource.base.UserDataDataSource
import app.clecia.matrix.domain.model.UserData
import app.clecia.matrix.domain.model.database.DBUserData
import app.clecia.matrix.providers.database.dao.UserDataDao
import app.clecia.matrix.providers.database.table.UserDataTable
import org.jetbrains.exposed.sql.SchemaUtils
import org.koin.core.annotation.Single

/**
 * 03 / 02 / 2023
 */
@Single
class UserDataDataSourceImpl : UserDataDataSource {
	init {
		cleciaTransaction {
			SchemaUtils.create(UserDataTable)
			SchemaUtils.createMissingTablesAndColumns(UserDataTable)
		}
	}

	private fun DBUserData.toData() =
		UserData(id.value, language, isTOSAgreed, isAdult)

	private fun getDB(userId: String) =
		UserDataDao.findById(userId)

	override fun get(userId: String): UserData? = cleciaTransaction {
		getDB(userId)?.toData()
	}

	override fun insert(userData: UserData) {
		cleciaTransaction {
			UserDataDao.new(userData.id) {
				language = userData.language
				isTOSAgreed = userData.isTOSAgreed
				isAdult = userData.isAdult
			}
		}
	}

	override fun update(userData: UserData) {
		cleciaTransaction {
			val data = getDB(userData.id)!!

			if (data.language != userData.language)
				data.language = userData.language

			if (data.isTOSAgreed != userData.isTOSAgreed)
				data.isTOSAgreed = userData.isTOSAgreed

			if (data.isAdult != userData.isAdult)
				data.isAdult = userData.isAdult
		}
	}
}