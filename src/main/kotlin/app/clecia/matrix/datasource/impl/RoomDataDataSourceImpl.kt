package app.clecia.matrix.datasource.impl

import app.clecia.matrix.common.ext.cleciaTransaction
import app.clecia.matrix.datasource.base.RoomDataDataSource
import app.clecia.matrix.domain.model.RoomData
import app.clecia.matrix.domain.model.database.DBRoomData
import app.clecia.matrix.providers.database.dao.RoomDataDao
import app.clecia.matrix.providers.database.table.RoomDataTable
import org.jetbrains.exposed.sql.SchemaUtils
import org.koin.core.annotation.Single

/**
 * 23 / 02 / 2023
 */
@Single
class RoomDataDataSourceImpl : RoomDataDataSource {
	init {
		cleciaTransaction {
			SchemaUtils.create(RoomDataTable)
			SchemaUtils.createMissingTablesAndColumns(RoomDataTable)
		}
	}

	fun DBRoomData.toData() =
		RoomData(id.value, language, isLewd)

	private fun getDB(roomId: String): DBRoomData? =
		RoomDataDao.findById(roomId)

	override fun get(roomId: String): RoomData? = cleciaTransaction {
		getDB(roomId)?.toData()
	}

	override fun insert(roomData: RoomData) {
		cleciaTransaction {
			RoomDataDao.new(roomData.id) {
				language = roomData.language
				isLewd = roomData.isLewd
			}
		}
	}

	override fun update(roomData: RoomData) {
		cleciaTransaction {
			val data = getDB(roomData.id)!!

			if (data.language != roomData.language)
				data.language = roomData.language

			if (data.isLewd != roomData.isLewd)
				data.isLewd = roomData.isLewd
		}
	}
}