package app.clecia.matrix.datasource.base

import app.clecia.matrix.domain.model.RoomData

/**
 * 23 / 02 / 2023
 */
interface RoomDataDataSource {
	fun get(roomId: String): RoomData?
	fun insert(roomData: RoomData)
	fun update(roomData: RoomData)
}