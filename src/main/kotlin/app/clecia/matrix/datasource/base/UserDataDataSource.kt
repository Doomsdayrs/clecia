package app.clecia.matrix.datasource.base

import app.clecia.matrix.domain.model.UserData

/**
 * 03 / 02 / 2023
 */
interface UserDataDataSource {
	fun get(userId: String): UserData?
	fun insert(userData: UserData)
	fun update(userData: UserData)
}