package app.clecia.matrix.backend

import app.clecia.matrix.domain.repository.base.WordFilterRepository
import kotlinx.coroutines.flow.first
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.message.notice
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 28 / 06 / 2022
 *
 * Handles and controls filters
 */
class FilterHandler(
	private val client: MatrixClient,
) : KoinComponent {
	private val wordFilterRepo: WordFilterRepository by inject()

	/**
	 * Handle a message event, and checks if it passes word filtering
	 *
	 * @param event
	 * @return True to continue processing, false if incorrect
	 */
	suspend fun handle(event: TimelineEvent): Boolean {
		if (event.content?.isSuccess == true) {
			val eventContent = event.content?.getOrNull()
			if (eventContent is RoomMessageEventContent) {
				val filters = wordFilterRepo.get(event.roomId.full).map { it.regex.toRegex() }
				val body = eventContent.body

				// Check if any of the filters finds a match in the body
				var matchingRegex: Regex? = null
				for (filter in filters) {
					if (body.matches(filter)) {
						matchingRegex = filter
						break
					}
				}
				if (matchingRegex != null) {
					val canRedact = client.user.canRedactEvent(event.roomId, event.eventId).first()
					if (canRedact) {
						client.api.rooms.redactEvent(
							event.roomId,
							event.eventId,
							"Contained word that was blocked"
						)

						client.room.sendMessage(event.roomId) {
							notice("Your message contained a blocked word, Please rewrite.")
						}
					} else {
						client.room.sendMessage(event.roomId) {
							notice("Cannot delete message containing blocked word, Contact moderation.")
						}
					}
					return false
				}
			}
		}

		return true
	}
}