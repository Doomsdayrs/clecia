package app.clecia.matrix.backend

import app.clecia.matrix.common.ext.takeUntilTimeout
import app.clecia.matrix.domain.repository.base.UserDataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.firstOrNull
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.RoomService
import net.folivo.trixnity.client.room.message.MessageBuilder
import net.folivo.trixnity.client.room.message.notice
import net.folivo.trixnity.client.room.message.thread
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.store.relatesTo
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 02 / 02 / 2023
 */
class TOSHandler(
	private val client: MatrixClient
) : KoinComponent {
	private val userDataRepo by inject<UserDataRepository>()

	/**
	 * Get replies to a specific event.
	 */
	fun repliesFor(event: TimelineEvent): Flow<TimelineEvent> =
		client.room.getTimelineEventsFromNowOn().filter {
			// Ensure it is the same room to save processing power
			it.roomId == event.roomId &&
					// Only get replies / thread events in relation to the [event]
					it.relatesTo?.replyTo?.eventId == event.eventId ||
					it.relatesTo?.eventId == event.eventId
		}

	/**
	 * Works on [RoomService.sendMessage].
	 *
	 * Sends a message, then gets the event for said message.
	 *
	 * @return TimelineEvent of the message, or null if something went wrong.
	 */
	suspend fun RoomService.dispatchMessage(
		roomId: RoomId,
		keepMediaInCache: Boolean = true,
		timeout: Long = 5000,
		builder: suspend MessageBuilder.() -> Unit
	): TimelineEvent? {
		val transactionId = sendMessage(roomId, keepMediaInCache, builder)
		val message = client.room.getTimelineEventsFromNowOn()
			.takeUntilTimeout(timeout)
			.firstOrNull {
				it.event.unsigned?.transactionId == transactionId
			}
		return message
	}


	/**
	 * @return true to continue, false otherwise
	 */
	suspend fun handle(event: TimelineEvent): Boolean {
		// TODO localization
		val userId = event.event.sender
		val userData = userDataRepo.get(userId.full)
		if (!userData.isTOSAgreed) {
			val myMessage = client.room.dispatchMessage(event.roomId) {
				notice(
					"""
					To use this bot, please agree to the following terms of service.

					- Clecia will store your preference data.
					- Doomsdayrs is not responsible for any damages caused by Clecia due to misuse.
					- Doomsdayrs reserves the right to disable your ability to use Clecia and or remove Clecia from any of your rooms and or spaces.

					Agree in 1 minute by replying with an $EMOJI_THUMBS_UP or "yes".
					""".trimIndent()
				)
				thread(event)
			}
			if (myMessage != null) {
				val replyEvent = repliesFor(myMessage)
					.takeUntilTimeout(60 * 1000)
					.firstOrNull { timelineEvent ->
						if (timelineEvent.content?.isSuccess == true) {
							when (val content = timelineEvent.content!!.getOrThrow()) {
								is RoomMessageEventContent.TextMessageEventContent -> {
									if (content.body.contains(EMOJI_THUMBS_UP) || AGREEMENTS.any { it == content.body })
										return@firstOrNull true
								}

								is RoomMessageEventContent.EmoteMessageEventContent -> {
									if (content.body.contains(EMOJI_THUMBS_UP))
										return@firstOrNull true
								}

								is RoomMessageEventContent.UnknownRoomMessageEventContent -> {
									if (content.body == EMOJI_THUMBS_UP || AGREEMENTS.any { it == content.body }) {
										return@firstOrNull true
									}
								}
							}
						}
						false
					}
				if (replyEvent != null) {
					userDataRepo.update(userData.copy(isTOSAgreed = true))
					client.room.sendMessage(event.roomId) {
						notice("Thank you for agreeing to the TOS")
						thread(replyEvent)
					}
					return true
				}
				return false
			} else {
				println("ERROR: Message not found to continue transaction, investigate state.")
				return false
			}
		}

		return true
	}

	companion object {
		const val EMOJI_THUMBS_UP = "👍"
		val AGREEMENTS = arrayOf(
			"yes",
			"y",
			"да",
			"si",
			"是的",
		)
	}
}