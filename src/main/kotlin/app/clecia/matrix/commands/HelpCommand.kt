package app.clecia.matrix.commands

import app.clecia.matrix.common.Markdown
import app.clecia.matrix.common.markdown
import app.clecia.matrix.domain.repository.base.RoomDataRepository
import me.doomsdayrs.trixnityx.commands.CommandDefineScope
import me.doomsdayrs.trixnityx.commands.CommandHandler
import me.doomsdayrs.trixnityx.commands.command
import me.doomsdayrs.trixnityx.commands.commands
import net.folivo.trixnity.client.room.message.text
import net.folivo.trixnity.client.room.message.thread
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 29 / 01 / 2023
 */
fun KoinComponent.helpCommand(handler: CommandHandler): CommandDefineScope {
	val roomDataRepo by inject<RoomDataRepository>()

	return commands {
		command {
			names["eng"] = "help"
			descriptions["eng"] = "Get help about the bot"

			var globalHelp: Markdown? = null;

			action {
				val language = roomDataRepo.get(event.roomId.full).language

				if (arguments.isEmpty()) {
					if (globalHelp == null) {
						globalHelp = markdown {
							h1("Clecia Help")
							handler.getCommands().forEach { command ->
								h2(command.names[language].toString())
								p(command.descriptions[language] ?: "¯\\_(ツ)_/¯")
							}
						}
					}
					respond(asReply = false) {
						text(
							body = globalHelp!!.content,
							format = "org.matrix.custom.html",
							formattedBody = globalHelp!!.html
						)
						thread(event, true)
					}
				} else {
					val specificCommandName = arguments.first()
					val specificCommand =
						handler.getCommands().find { it.names.containsValue(specificCommandName) }

					if (specificCommand == null) {
						respond {
							text("No such command found")
						}
					} else {
						val markdown = markdown {
							h1(specificCommandName)
							p(specificCommand.descriptions[language] ?: "¯\\_(ツ)_/¯")
						}

						respond {
							text(
								body = markdown.content,
								format = "org.matrix.custom.html",
								formattedBody = markdown.html
							)
						}
					}
				}
			}
		}
	}
}