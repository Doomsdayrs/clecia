package app.clecia.matrix.commands

import app.clecia.matrix.common.magicballReplies
import app.clecia.matrix.common.victoryTexts
import me.doomsdayrs.trixnityx.commands.PREFIX
import me.doomsdayrs.trixnityx.commands.command
import me.doomsdayrs.trixnityx.commands.commands
import net.folivo.trixnity.client.room.message.notice
import net.folivo.trixnity.client.room.message.text
import kotlin.random.Random

data class Dice(val sides: Int, val overrideResult: Int? = null)

fun parseDiceInput(input: String): List<Dice> {
	val dice = ArrayList<Dice>()
	// If d is the first character, assume that dice count is 1
	val diceCount = if (input.indexOf('d') == 0) 1 else {
		input.substringBefore('d').toInt()
	}

	val afterD = input.substringAfter('d')

	val diceSides: Int
	if (afterD.contains('+')) {
		diceSides = afterD.substringBefore('+').trim().toInt()
		val afterPlus = afterD.substringAfter('+')
		if (afterPlus.contains("d")) {
			dice += parseDiceInput(afterPlus)
		} else dice += Dice(0, afterPlus.toInt()) // + will count as predefined
	} else {
		diceSides = afterD.toInt()
	}
	for (i in 0 until diceCount) {
		dice += Dice(diceSides)
	}
	return dice
}

/**
 * 28 / 06 / 2022
 */
val funCommands = commands {
	command {
		names["eng"] = "magicball"
		descriptions["eng"] = "A magic ball that will help you decide on things"

		action {
			val selection = magicballReplies.random()

			respond {
				text(selection)
			}
		}
	}
	command {
		names["eng"] = "roll"
		descriptions["eng"] =
			"""
				Roll (a) dice.
				By default rolls 1d6.
				To roll differently, follow this example: `${PREFIX}roll 2d10`.
			""".trimIndent()

		action {
			val dice: List<Dice>
			val input: String
			if (arguments.isEmpty()) {
				input = "1d6"
				dice = listOf(Dice(6))
			} else {
				input = arguments.joinToString("")
				if (input.contains("d")) {
					try {
						dice = parseDiceInput(input)
					} catch (e: NumberFormatException) {
						respond {
							notice("Invalid input ${e.message}")
						}
						return@action
					}
				} else {
					respond {
						notice("Invalid input: $input")
					}
					return@action
				}
			}
			val results = ArrayList<Int>()
			dice.forEach {
				results += it.overrideResult ?: (Random.nextInt(it.sides) + 1)
			}
			respond {
				text("$input: ${results.sum()} from $results")
			}

		}
	}
	command {
		names["eng"] = "fight"
		descriptions["eng"] = "Fight another user"

		action {
			val user = getAuthor()?.name

			if (arguments.isEmpty()) {
				respond {
					text("No one to fight")
				}
				return@action
			} else if (arguments.size > 1) {
				respond {
					text("You can only fight one at a time")
				}
				return@action
			}
			val target = arguments[0]
			val victory = victoryTexts.random()

			respond {
				if (Random.nextBoolean())
					text(victory.format(user, target))
				else text(victory.format(target, user))
			}
		}
	}
}