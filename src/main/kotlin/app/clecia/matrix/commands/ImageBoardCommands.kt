package app.clecia.matrix.commands

import app.clecia.matrix.common.markdown
import app.clecia.matrix.domain.repository.base.RoomDataRepository
import com.gitlab.doomsdayrs.lib.imageboardkt.api.ImageBoardAPI
import com.gitlab.doomsdayrs.lib.imageboardkt.ktor.api.*
import com.gitlab.doomsdayrs.lib.imageboardkt.model.BoardImage
import com.gitlab.doomsdayrs.lib.imageboardkt.model.Rating
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.ContentType
import io.ktor.http.fromFileExtension
import me.doomsdayrs.trixnityx.commands.CommandAction
import me.doomsdayrs.trixnityx.commands.command
import me.doomsdayrs.trixnityx.commands.commands
import me.doomsdayrs.trixnityx.dispatchMessage
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.message.*
import net.folivo.trixnity.core.toByteArrayFlow
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.random.Random

/**
 * 11 / 02 / 2023
 */
fun KoinComponent.imageBoardCommands(httpClient: HttpClient) = commands {
	val roomDataRepo by inject<RoomDataRepository>()

	suspend fun <T : BoardImage> CommandAction.imageBoardAction(
		api: ImageBoardAPI<T>,
		isNSFW: Boolean = true
	) {
		val image =
			api.search(
				10,
				query = arguments.joinToString(" "),
				page = Random.nextInt(100) + 1,
				rating = if (isNSFW) null else Rating.SAFE
			).filter { it.imageURL != null }.randomOrNull()

		if (image == null) {
			respond {
				text("Failed to find an image")
			}
			return
		}

		val imageURL = image.imageURL!!

		val type = ContentType.fromFileExtension(imageURL.substringAfterLast("."))
			.firstOrNull() ?: ContentType.Image.Any

		val response = client.room.dispatchMessage(
			event.roomId
		) {
			if (type.contentType == "video")
				video(
					imageURL,
					httpClient.get(imageURL).bodyAsChannel().toByteArrayFlow(),
					type = type
				)
			else
				image(
					imageURL,
					httpClient.get(imageURL).bodyAsChannel().toByteArrayFlow(),
					type = type
				)

			reply(event)
		}

		if (response != null) {
			client.room.dispatchMessage(event.roomId) {
				thread(response)
				val md = markdown {
					b("Source:")
					p(image.postURL)
					b("Tags:")
					p(image.tags.joinToString(", "))
				}
				text(
					body = md.content,
					format = "org.matrix.custom.html",
					formattedBody = md.html
				)
			}
		}
	}

	suspend fun CommandAction.isRoomLewd(): Boolean =
		roomDataRepo.get(event.roomId.full).isLewd ||
				getRoom()?.isDirect ?: false

	command {
		names["eng"] = "danbooru"
		descriptions["eng"] = "Search DanBooru"

		action {
			imageBoardAction(DanBooruAPI(httpClient), isRoomLewd())
		}
	}

	command {
		names["eng"] = "e621"
		descriptions["eng"] = "(Lewd) Search e621"

		action {
			if (isRoomLewd())
				imageBoardAction(E621API(httpClient))
			else
				respond {
					text(WARN_NOT_LEWD_ROOM)
				}
		}
	}

	command {
		names["eng"] = "e926"
		descriptions["eng"] = "Search e926"

		action {
			imageBoardAction(E926API(httpClient), isRoomLewd())
		}
	}

	command {
		names["eng"] = "gelbooru"
		descriptions["eng"] = "(Lewd) Search GelBooru"

		action {
			if (isRoomLewd())
				imageBoardAction(GelBooruAPI(httpClient))
			else respond {
				text(WARN_NOT_LEWD_ROOM)
			}
		}
	}

	command {
		names["eng"] = "hypnohub"
		descriptions["eng"] = "(Lewd) Search HypnoHub"

		action {
			if (isRoomLewd())
				imageBoardAction(HypnoHubAPI(httpClient))
			else
				respond {
					text(WARN_NOT_LEWD_ROOM)
				}
		}
	}

	command {
		names["eng"] = "konachan"
		descriptions["eng"] = "(Lewd) Search KonaChan"

		action {
			imageBoardAction(KonaChanAPI(httpClient), isRoomLewd())
		}
	}

	command {
		names["eng"] = "realbooru"
		descriptions["eng"] = "(Lewd) Search RealBooru"

		action {
			if (isRoomLewd()) {
				imageBoardAction(RealBooruAPI(httpClient))
			} else {
				respond {
					text(WARN_NOT_LEWD_ROOM)
				}
			}
		}
	}

	command {
		names["eng"] = "rule34"
		descriptions["eng"] = "(Lewd) Search rule34"

		action {
			if (isRoomLewd()) {
				imageBoardAction(Rule34API(httpClient))
			} else {
				respond {
					text(WARN_NOT_LEWD_ROOM)
				}
			}
		}
	}

	command {
		names["eng"] = "safebooru"
		descriptions["eng"] = "Search SafeBooru"

		action {
			imageBoardAction(SafeBooruAPI(httpClient))
		}
	}

	command {
		names["eng"] = "xbooru"
		descriptions["eng"] = "(Lewd) Search XBooru"

		action {
			if (isRoomLewd()) {
				imageBoardAction(XBooruAPI(httpClient))
			} else {
				respond {
					notice(WARN_NOT_LEWD_ROOM)
				}
			}
		}
	}

	command {
		names["eng"] = "yandere"
		descriptions["eng"] = "Search yandere"

		action {
			imageBoardAction(YandereAPI(httpClient), isRoomLewd())
		}
	}
}

const val WARN_NOT_LEWD_ROOM = "This room is not marked as lewd."