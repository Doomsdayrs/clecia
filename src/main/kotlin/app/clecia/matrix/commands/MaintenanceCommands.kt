package app.clecia.matrix.commands

import app.clecia.matrix.isBotRunning
import me.doomsdayrs.trixnityx.commands.command
import me.doomsdayrs.trixnityx.commands.commands
import net.folivo.trixnity.client.room.message.text
import net.folivo.trixnity.core.model.UserId

/**
 * 28 / 06 / 2022
 */

val maintenanceCommands = commands {
	command {
		names["eng"] = "stop"
		names["ger"] = "halt"
		names["rus"] = "останавливаться"
		action {

			if (event.event.sender != UserId("@doomsdayrs:matrix.org")) {
				respond {
					text("You are not the admin")
				}
				return@action
			}
			respond {
				text("Goodbye")
			}
			isBotRunning.value = false
		}
	}
}