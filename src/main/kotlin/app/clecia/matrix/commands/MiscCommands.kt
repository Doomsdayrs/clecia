package app.clecia.matrix.commands

import app.clecia.matrix.common.markdown
import app.clecia.matrix.domain.repository.base.StatusRepository
import me.doomsdayrs.trixnityx.commands.CommandDefineScope
import me.doomsdayrs.trixnityx.commands.command
import me.doomsdayrs.trixnityx.commands.commands
import net.folivo.trixnity.client.room.message.text
import net.folivo.trixnity.client.room.message.thread
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 28 / 06 / 2022
 */
fun KoinComponent.miscCommands(): CommandDefineScope {
	val statusRepo by inject<StatusRepository>()

	return commands {
		command {
			names["eng"] = "ping"
			names["ger"] = "klingeln"
			names["rus"] = "пинг"

			action {
				val time = System.currentTimeMillis()
				val eventTime = event.event.originTimestamp

				val difference = time - eventTime

				respond {
					text("Pong: $difference ms")
				}
			}
		}

		command {
			names["eng"] = "status"
			descriptions["eng"] = "Query bot status"

			action {
				respond(asReply = false) {
					val markdown = markdown {
						h1("Status")
						p("Uptime:\t\t" + statusRepo.getUpTimeMs())
						p("Commands Executed:\t" + statusRepo.getCommandsExecuted())
					}

					text(
						body = markdown.content,
						format = "org.matrix.custom.html",
						formattedBody = markdown.html
					)
					thread(event, true)
				}
			}
		}

		command {
			names["eng"] = "whoami"
			action {
				val user = getAuthor()
				respond {
					text(user?.name ?: "I do not know")
				}
			}
		}



		command {
			names["eng"] = "argTest"
			action {
				respond {
					text("Arguments: $arguments")
				}
			}
		}
	}
}