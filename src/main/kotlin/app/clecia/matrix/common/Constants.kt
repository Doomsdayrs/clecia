package app.clecia.matrix.common

/**
 * 29 / 01 / 2023
 */

val magicballReplies = arrayOf(
	// Positive
	"Certainly",
	"Undoublty",
	"It has been decided so",
	"Def",
	"Bet on it",
	"I see so",
	"Likely",
	"You have the luck",
	"Yeah",
	"Material conditions say so",

	// Non committal
	"Material conditions are too complicated",
	"Maybe later",
	"Idk",
	"Focus and ask again",
	"Sorry, what did you say",

	// Negative
	"Nope",
	"Fuck that",
	"Negative",
	"Material conditions are not in your favour",
	"Better luck in your next life"
)

val victoryTexts = arrayOf(
	"%s smashes %s",
	"%s decapitates %s",
	"%s runs over %s",
	"%s rolls over %s in a tank",
	"%s nukes %s",
	"%s no scopes %s",
	"%s smothers %s",
)