package app.clecia.matrix.common

import kotlinx.html.*
import kotlinx.html.stream.createHTML

/**
 * 31 / 01 / 2023
 *
 * TODO trixnityx-markdown
 */

@DslMarker
annotation class MarkdownDsl;

data class Markdown(val content: String, val html: String) {

}

class MarkdownScope {
	var content: StringBuffer = StringBuffer()
	var html = createHTML()
	var htmlBody: BODY = BODY(attributesMapOf("class", null), html)

	fun h1(string: String) {
		content.appendLine("# $string")
		htmlBody.visit {
			h1 {
				text(string)
			}
		}
	}

	fun h2(string: String) {
		content.appendLine("## $string")
		htmlBody.visit {
			h2 {
				text(string)
			}
		}
	}

	fun h3(string: String) {
		content.appendLine("### $string")
		htmlBody.visit {
			h3 {
				text(string)
			}
		}
	}

	fun b(string: String) {
		content.appendLine("**$string**")
		htmlBody.visit {
			b {
				text(string)
			}
		}
	}

	fun i(string: String) {
		content.appendLine("*$string*")
		htmlBody.visit {
			i {
				text(string)
			}
		}
	}

	fun bi(string: String) {
		content.appendLine("***$string***")
		htmlBody.visit {
			b {
				i {
					text(string)
				}
			}
		}
	}

	fun p(string: String) {
		content.appendLine(string)
		htmlBody.visit {
			p {
				text(string)
			}
		}
	}
}

@MarkdownDsl
fun markdown(build: MarkdownScope.() -> Unit): Markdown {
	val scope = MarkdownScope()

	scope.build()

	return Markdown(
		content = scope.content.toString(),
		scope.htmlBody.visitAndFinalize(scope.html) {}
	)
}