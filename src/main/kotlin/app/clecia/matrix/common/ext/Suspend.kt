package app.clecia.matrix.common.ext

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * 23 / 02 / 2023
 */
suspend fun <R> onIO(block: suspend CoroutineScope.() -> R): R = withContext(Dispatchers.IO, block)