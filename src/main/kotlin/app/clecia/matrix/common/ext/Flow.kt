package app.clecia.matrix.common.ext

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.launch

/**
 * 03 / 02 / 2023
 */
fun <T> Flow<T>.takeUntilTimeout(timeoutMillis: Long) = channelFlow {
	val collector = launch {
		collect {
			send(it)
		}
		close()
	}
	delay(timeoutMillis)
	collector.cancel()
	close()
}