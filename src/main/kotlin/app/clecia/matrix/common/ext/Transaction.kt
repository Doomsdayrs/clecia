package app.clecia.matrix.common.ext

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * 12 / 05 / 2023
 */
fun <T> cleciaTransaction(db: Database? = null, statement: Transaction.() -> T): T =
	transaction(db) {
		addLogger(StdOutSqlLogger)
		statement()
	}