package app.clecia.matrix

import app.clecia.matrix.backend.FilterHandler
import app.clecia.matrix.backend.TOSHandler
import app.clecia.matrix.commands.*
import app.clecia.matrix.domain.repository.base.StatusRepository
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.plugins.UserAgent
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.logging.SIMPLE
import io.ktor.http.Url
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.datetime.Clock
import me.doomsdayrs.trixnityx.commands.CommandHandler
import me.doomsdayrs.trixnityx.commands.PREFIX
import me.doomsdayrs.trixnityx.joinAllInvites
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.fromStore
import net.folivo.trixnity.client.login
import net.folivo.trixnity.client.media.okio.OkioMediaStore
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.store.repository.exposed.createExposedRepositoriesModule
import net.folivo.trixnity.clientserverapi.model.authentication.IdentifierType
import okio.Path.Companion.toPath
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.ksp.generated.defaultModule

/**
 * 25 / 04 / 2022
 */
var isBotRunning = MutableStateFlow(true)

class Core : KoinComponent {

	private val statusRepo by inject<StatusRepository>()

	suspend fun main(args: Array<String>) {
		startKoin {
			defaultModule()
		}
		statusRepo.setStartTimeMs(Clock.System.now().toEpochMilliseconds())

		val scope = CoroutineScope(Dispatchers.IO)
		val commandScope = CoroutineScope(Dispatchers.IO)
		val repoModule = createExposedRepositoriesModule(
			Database.connect("jdbc:h2:./Data/database.db", driver = "org.h2.Driver")
		)
		val store = OkioMediaStore(basePath = "./Data/Media/".toPath())

		transaction {  }

		val matrixClient = MatrixClient.fromStore(
			scope = scope,
			repositoriesModule = repoModule,
			mediaStore = store
		).getOrThrow() ?: MatrixClient.login(
			baseUrl = Url("https://matrix.org"),
			identifier = IdentifierType.User(BuildConfig.username),
			password = BuildConfig.password,
			mediaStore = store,
			repositoriesModule = repoModule,
			scope = scope
		).getOrThrow()

		val commandHandler = CommandHandler(matrixClient)

		val filterHandler = FilterHandler(matrixClient)
		val tosHandler = TOSHandler(matrixClient)

		PREFIX = "c!"
		if (System.getenv("DEBUG") == "true")
			PREFIX = "t!"

		matrixClient.startSync()

		commandHandler.register(funCommands)
		commandHandler.register(maintenanceCommands)
		commandHandler.register(miscCommands())
		val client = HttpClient(Apache) {
			install(Logging) {
				logger = Logger.SIMPLE
				level = LogLevel.INFO
			}
			install(UserAgent) {
				agent = "Clecia/0.0 (by Doomsdayrs on GitLab)"
			}
		}
		commandHandler.register(imageBoardCommands(client))
		commandHandler.register(helpCommand(commandHandler))
		commandHandler.addOnPreCommandListener {
			statusRepo.incrementCommandsExecuted()
		}
		commandHandler.setInterceptor { event, _ ->
			runBlocking(Dispatchers.IO) {
				tosHandler.handle(event)
			}
		}

		supervisorScope {
			commandScope.apply {
				launch {
					supervisorScope {
						matrixClient.room.getTimelineEventsFromNowOn().collect { timelineEvent ->
							launch {
								//if (!filterHandler.handle(timelineEvent)) return@launch

								commandHandler.feedEvent(timelineEvent)
							}
						}
					}
				}
				launch {
					matrixClient.joinAllInvites(interject = {
						println("Joining room ${it.name} : ${it.roomId}")
						true
					}, callback = { result ->
						result.fold(onSuccess = { println("join success") },
							onFailure = { println("join failure: $it") })
					})
				}
			}
		}

		isBotRunning.first { !it }

		matrixClient.cancelSync(true)
	}

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			runBlocking(Dispatchers.IO) {
				Core().main(args)
			}
		}
	}
}

