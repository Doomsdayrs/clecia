package app.clecia.matrix.providers.database.table

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

/**
 * 03 / 02 / 2023
 */
object RoomDataTable : IdTable<String>() {
	override val tableName: String = "clecia_room"
	override val id: Column<EntityID<String>> = varchar("id", 50).entityId()
	val language: Column<String> = varchar("language", 3)
	val isLewd: Column<Boolean> = bool("is_lewd")
}