package app.clecia.matrix.providers.database.dao

import app.clecia.matrix.common.ext.StringEntityClass
import app.clecia.matrix.domain.model.database.DBUserData
import app.clecia.matrix.providers.database.table.UserDataTable

object UserDataDao : StringEntityClass<DBUserData>(UserDataTable, DBUserData::class.java)