package app.clecia.matrix.providers.database.dao

import app.clecia.matrix.common.ext.StringEntityClass
import app.clecia.matrix.domain.model.database.DBRoomData
import app.clecia.matrix.providers.database.table.RoomDataTable

/**
 * 23 / 02 / 2023
 */
object RoomDataDao : StringEntityClass<DBRoomData>(RoomDataTable, DBRoomData::class.java)