package app.clecia.matrix.providers.database.table

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

/**
 * 03 / 02 / 2023
 */
object UserDataTable : IdTable<String>() {
	override val tableName: String = "clecia_user"
	override val id: Column<EntityID<String>> = varchar("id", 50).entityId()
	override val primaryKey: PrimaryKey = PrimaryKey(id)
	val isTOSAgreed: Column<Boolean> = bool("agreed_to_tos")
	val language: Column<String> = varchar("language", 3)
	val isAdult: Column<Boolean> = bool("is_adult")
}