package app.clecia.matrix.domain.repository.base

import app.clecia.matrix.domain.model.UserData

/**
 * 28 / 06 / 2022
 *
 * SOT for all user data
 */
interface UserDataRepository {
	suspend fun get(userId: String): UserData
	suspend fun update(userData: UserData)
}