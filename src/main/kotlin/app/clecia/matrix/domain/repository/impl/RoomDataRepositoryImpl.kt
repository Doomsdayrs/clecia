package app.clecia.matrix.domain.repository.impl

import app.clecia.matrix.common.ext.onIO
import app.clecia.matrix.datasource.base.RoomDataDataSource
import app.clecia.matrix.domain.model.RoomData
import app.clecia.matrix.domain.repository.base.RoomDataRepository
import org.koin.core.annotation.Single
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 23 / 02 / 2023
 */
@Single
class RoomDataRepositoryImpl : RoomDataRepository, KoinComponent {
	private val dataSource by inject<RoomDataDataSource>()

	override suspend fun get(roomId: String): RoomData = onIO {
		var data = dataSource.get(roomId)

		if (data == null) {
			data = RoomData(roomId)
			dataSource.insert(data)
			return@onIO data
		}

		return@onIO data
	}

}