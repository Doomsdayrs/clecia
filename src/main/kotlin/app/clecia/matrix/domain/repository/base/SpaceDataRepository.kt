package app.clecia.matrix.domain.repository.base

import app.clecia.matrix.domain.model.SpaceData

/**
 * 28 / 06 / 2022
 */
interface SpaceDataRepository {
	suspend fun get(spaceId: String): SpaceData
}