package app.clecia.matrix.domain.repository.base

import app.clecia.matrix.domain.model.RoomData

/**
 * 28 / 06 / 2022
 *
 * SOT for all room data
 */
interface RoomDataRepository {
	suspend fun get(roomId: String): RoomData
}