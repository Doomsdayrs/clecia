package app.clecia.matrix.domain.repository.impl.fake

import app.clecia.matrix.domain.model.WordFilter
import app.clecia.matrix.domain.repository.base.WordFilterRepository
import org.koin.core.annotation.Single
import java.util.concurrent.ConcurrentHashMap

/**
 * 29 / 01 / 2023
 */
@Single
class MemoryWordFilterRepository : WordFilterRepository {
	private val map = ConcurrentHashMap<String, ArrayList<WordFilter>>()

	override suspend fun get(roomId: String): List<WordFilter> =
		listOf(
			WordFilter(
				0,
				listOf(roomId),
				"pineapple"
			)
		)

	override suspend fun add(roomId: String, filter: String) {
		TODO("Not yet implemented")
	}

	override suspend fun update(filterId: Long, filter: String) {
		TODO("Not yet implemented")
	}

	override suspend fun remove(filter: WordFilter) {
		TODO("Not yet implemented")
	}
}