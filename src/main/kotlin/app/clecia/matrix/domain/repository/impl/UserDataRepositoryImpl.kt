package app.clecia.matrix.domain.repository.impl

import app.clecia.matrix.common.ext.onIO
import app.clecia.matrix.datasource.base.UserDataDataSource
import app.clecia.matrix.domain.model.UserData
import app.clecia.matrix.domain.repository.base.UserDataRepository
import org.koin.core.annotation.Single
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * 23 / 02 / 2023
 */
@Single
class UserDataRepositoryImpl : UserDataRepository, KoinComponent {
	private val dataSource by inject<UserDataDataSource>()

	override suspend fun get(userId: String): UserData = onIO {
		var data = dataSource.get(userId)

		if (data == null) {
			data = UserData(userId)
			dataSource.insert(data)
			return@onIO data
		}

		return@onIO data
	}

	override suspend fun update(userData: UserData) = onIO {
		dataSource.update(userData)
	}
}