package app.clecia.matrix.domain.repository.base

import app.clecia.matrix.domain.model.WordFilter

/**
 * 28 / 06 / 2022
 */
interface WordFilterRepository {
	/**
	 * Get filters for a specific space
	 */
	suspend fun get(roomId: String): List<WordFilter>

	/**
	 * Apply filter to section
	 *
	 * @param roomId Room id
	 * @param filter regex filter
	 */
	suspend fun add(roomId: String, filter: String)

	/**
	 * Update a filter regex
	 *
	 * @param filterId ID of the filter
	 * @param filter new regex
	 */
	suspend fun update(filterId: Long, filter: String)

	/**
	 * Remove a filter
	 */
	suspend fun remove(filter: WordFilter)
}