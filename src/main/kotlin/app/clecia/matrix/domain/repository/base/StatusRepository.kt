package app.clecia.matrix.domain.repository.base

import kotlin.time.Duration

/**
 * 31 / 01 / 2023
 */
interface StatusRepository {
	fun getUpTimeMs(): Duration
	fun setStartTimeMs(time: Long)

	fun getCommandsExecuted(): Int

	fun incrementCommandsExecuted()
}