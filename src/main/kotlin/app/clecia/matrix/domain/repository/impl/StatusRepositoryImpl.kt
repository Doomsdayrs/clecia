package app.clecia.matrix.domain.repository.impl

import app.clecia.matrix.domain.repository.base.StatusRepository
import kotlinx.datetime.Clock
import org.koin.core.annotation.Single
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

/**
 * 31 / 01 / 2023
 */
@Single
class StatusRepositoryImpl : StatusRepository {
	private var startTime: Long = 0
	private var commandsExecuted = 0

	override fun getUpTimeMs(): Duration =
		(Clock.System.now().toEpochMilliseconds() - startTime).toDuration(DurationUnit.MILLISECONDS)

	override fun setStartTimeMs(time: Long) {
		startTime = time
	}

	override fun getCommandsExecuted(): Int =
		commandsExecuted

	override fun incrementCommandsExecuted() {
		commandsExecuted += 1
	}
}