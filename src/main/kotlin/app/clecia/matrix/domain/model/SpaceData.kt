package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 * @param id ID of the space
 */
data class SpaceData(
	val id: String,
)
