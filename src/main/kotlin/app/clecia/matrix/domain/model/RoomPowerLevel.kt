package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 */
data class RoomPowerLevel(
	val roomId: String,
	val level: Int,
	val name: String
)
