package app.clecia.matrix.domain.model.database

import app.clecia.matrix.common.ext.StringEntity
import app.clecia.matrix.providers.database.table.RoomDataTable
import org.jetbrains.exposed.dao.id.EntityID

/**
 * 23 / 02 / 2023
 */
class DBRoomData(id: EntityID<String>) : StringEntity(id) {
	var language by RoomDataTable.language
	var isLewd by RoomDataTable.isLewd
}