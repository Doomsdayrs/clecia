package app.clecia.matrix.domain.model.database

import app.clecia.matrix.common.ext.StringEntity
import app.clecia.matrix.providers.database.table.UserDataTable
import org.jetbrains.exposed.dao.id.EntityID

/**
 * 03 / 02 / 2023
 */
class DBUserData(id: EntityID<String>) : StringEntity(id) {
	var isTOSAgreed by UserDataTable.isTOSAgreed
	var language by UserDataTable.language
	var isAdult by UserDataTable.isAdult
}