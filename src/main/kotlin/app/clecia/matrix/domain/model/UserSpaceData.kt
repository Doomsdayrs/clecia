package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 */
data class UserSpaceData(
	val id: String,
	val spaceId: String
)
