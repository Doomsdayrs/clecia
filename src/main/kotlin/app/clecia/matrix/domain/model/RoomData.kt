package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 *
 * @param id identification of the room.
 * @param language language of the room.
 * @param isLewd is the room lewd or not.
 */
data class RoomData(
	val id: String,
	val language: String = "eng",
	val isLewd: Boolean = false
)
