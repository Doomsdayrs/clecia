package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 */
data class WordFilter(
	val id: Long,
	val roomIds: List<String>,
	val regex: String,
)