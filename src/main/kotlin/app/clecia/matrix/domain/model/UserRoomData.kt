package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 */
data class UserRoomData(
	val id: String,
	val roomId: String
)
