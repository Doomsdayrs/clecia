package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 */
data class SpacePowerLevel(
	val spaceId: String,
	val level: Int,
	val name: String
)