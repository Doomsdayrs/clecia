package app.clecia.matrix.domain.model

/**
 * 28 / 06 / 2022
 *
 * @param language User preferred language
 * @param isTOSAgreed Has the user agreed to the TOS
 * @param isAdult Has the user confirmed they are an adult.
 */
data class UserData(
	val id: String,
	val language: String = "eng",
	val isTOSAgreed: Boolean = false,
	val isAdult: Boolean = false
)
