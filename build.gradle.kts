import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.*
val ktor_version: String by project

plugins {
	kotlin("jvm") version "1.8.0"
	application
	id("com.github.gmazzo.buildconfig") version "3.1.0"
	id("com.google.devtools.ksp") version "1.8.0-1.0.9"
}

group = "app.clecia.matrix"
version = "0.2.0"

application {
	mainClass.set("app.clecia.matrix.Core")
}

repositories {
	mavenCentral()
	mavenLocal()
	maven("https://jitpack.io")
}

buildConfig {
	this.packageName.set(group.toString())

	val props = Properties()
	props.load(File(rootDir, "login.properties").inputStream())

	buildConfigField("String", "VERSION", "\"$version\"")
	buildConfigField("String", "username", "\"${props["username"]}\"")
	buildConfigField("String", "password", "\"${props["password"]}\"")
}

sourceSets.main {
	java.srcDirs("build/generated/ksp/main/kotlin")
}

java {
	sourceCompatibility = JavaVersion.VERSION_17
	targetCompatibility = JavaVersion.VERSION_17
}

dependencies {
	testImplementation(kotlin("test"))

	// trixnity
	fun trixnity(module: String, version: String = "3.6.0") =
		"net.folivo:trixnity-$module:$version"
	implementation(trixnity("clientserverapi-client"))
	implementation(trixnity("client-repository-exposed"))
	implementation(trixnity("client-media-okio"))

	// database
	implementation("com.h2database:h2:2.1.214")
	implementation("org.jetbrains.exposed", "exposed-dao", "0.40.1")

	// ktor
	implementation("io.ktor:ktor-client-apache:$ktor_version")
	implementation("io.ktor:ktor-client-logging:$ktor_version")

	// Kotlinx
	implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
	implementation("org.jetbrains.kotlinx:kotlinx-html:0.8.0")

	implementation("com.gitlab.Doomsdayrs:trixnityx:1.0.1")
	implementation("com.gitlab.Doomsdayrs:trixnityx-commands:a0e25453")
	implementation("com.gitlab.doomsdayrs.lib:danbooru-kt-ktor:0.0.0")

	// koin
	fun koin(module: String, version: String = "3.2.0") =
		"io.insert-koin:koin-$module:$version"
	implementation(koin("core"))
	implementation(koin("annotations", "1.0.1"))
	ksp("io.insert-koin:koin-ksp-compiler:1.0.3")
	testImplementation(koin("test"))
}

tasks {
	test {
		useJUnitPlatform()
	}

	withType<KotlinCompile> {
		kotlinOptions.jvmTarget = "17"
	}

	register("installToServer") {
		val props = Properties()
		props.load(File(rootDir, "server.properties").inputStream())

		dependsOn(distTar)
		val user = props["USERNAME"]
		val filePrefix = "Clecia-$version"
		val fileName = "$filePrefix.tar"
		val srcFile = File(buildDir, "distributions/$fileName")
		val ip = props["IP"]
		doLast {
			Runtime.getRuntime().apply {

				println("Sending to server")
				exec("scp ${srcFile.absolutePath} $user@$ip:/tmp/").waitFor()

				println("Extracting")
				exec("ssh $user@$ip tar -xf /tmp/$fileName -C /tmp/").waitFor()

				println("Removing old jars")
				exec("ssh $user@$ip rm /opt/Clecia/lib/*.jar")

				println("Moving new files in")
				exec("ssh $user@$ip mv /tmp/$filePrefix/bin/* /opt/Clecia/bin/").waitFor()
				exec("ssh $user@$ip mv /tmp/$filePrefix/lib/* /opt/Clecia/lib/").waitFor()

				println("Cleaning")
				exec("ssh $user@$ip rm -rf /tmp/$filePrefix /tmp/$fileName").waitFor()
			}
		}
	}
}